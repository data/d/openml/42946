# OpenML dataset: dgf_123e1c18-37e0-4147-ad65-768320387800

https://www.openml.org/d/42946

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Liste des logements proposes en Airbnb sur Bordeaux

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42946) of an [OpenML dataset](https://www.openml.org/d/42946). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42946/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42946/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42946/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

